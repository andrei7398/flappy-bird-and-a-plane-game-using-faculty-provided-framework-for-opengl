#version 330

layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_normal;
layout(location = 2) in vec2 v_coord;
layout(location = 3) in vec3 v_color;

uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;
uniform float Clock;

out vec3 position;
out vec3 color;
out vec3 normal;

void main() {

	position = v_position * Clock;
	color = v_color * Clock;
	normal = v_normal;

	gl_Position = Projection * View * Model * vec4(v_position, 1);
}
