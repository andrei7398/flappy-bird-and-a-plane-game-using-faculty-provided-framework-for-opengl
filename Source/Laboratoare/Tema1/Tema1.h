#pragma once

#include <Component/SimpleScene.h>
#include <string>
#include <Core/Engine.h>

class Tema1 : public SimpleScene
{
public:
	Tema1();
	~Tema1();

	void Init() override;

private:
	void FrameStart() override;
	void Update(float deltaTimeSeconds) override;
	void FrameEnd() override;

	void ResetStats();

	void OnInputUpdate(float deltaTime, int mods) override;
	void OnKeyPress(int key, int mods) override;
	void OnKeyRelease(int key, int mods) override;
	void OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY) override;
	void OnMouseBtnPress(int mouseX, int mouseY, int button, int mods) override;
	void OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods) override;
	void OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY) override;
	void OnWindowResize(int width, int height) override;

	Mesh* CreateCircle(std::string name, glm::vec3 corner, int accuracy, float radius, glm::vec3 color, bool fill);
	Mesh* CreateTube(std::string name, int size, float outline, int upDown);
	Mesh* CreateCity(std::string name);
	Mesh* CreateGround(std::string name, int layer);
	Mesh* CreateBird(std::string name, int wings);
	Mesh* CreateCloudsAndForest(std::string name);

protected:
	glm::mat3 modelMatrix;
	glm::ivec2 resolution;

	// Tube pair (Ti, Tj) translates
	float translateX1, translateX2, translateX3, translateX4;
	float translateY1, translateY2, translateY3, translateY4;

	// Bird drop translate
	float translateY;

	// Grass translate for distance effect
	float translateGrass;

	// Maximum left translate for the sliding terrain before recenter
	float grass_Slide = 300.0f;

	// Clockwise rotation for bird drop - maximum M_PI/3
	float total_Rotate;
	float total_Rotate_aux;

	// For any circle creation
	float angle = M_PI / 10;

	// For bird weight center calculus => (max + min) / 2
	float max_X;
	float min_X;
	float max_Y;
	float min_Y;

	// Bird wings effect - change object when fly_time = fly_time_MAX || fly_time_MAX / 2
	int fly_time;

	// Start time to let player prepare, score and Space Press for jump
	int START_TIME;
	int SCORE;
	int SPACE_PRESS = 0;

	int totalTime = 0;
	int sizeOfTube = 100;

	// Used for collision calculus - to fit the bird and every tube in a rectangle
	struct Collision {
		float x;
		float y;
		float width;
		float height;
	};
	Collision bird;
	std::vector<Collision> tubes;
};
