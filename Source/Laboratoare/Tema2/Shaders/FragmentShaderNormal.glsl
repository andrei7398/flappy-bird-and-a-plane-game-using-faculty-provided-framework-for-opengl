#version 330

uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;

layout(location = 0) out vec3 color1;
layout(location = 1) out vec3 position1;

in vec3 color;
in vec3 position;
in vec3 normal;

void main() {

	color1 = color;
	position1 = position;
}