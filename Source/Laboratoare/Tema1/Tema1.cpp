// IULIAN-ANDREI SFETCU - 333CB - TEMA 1 - EGC

#include "Tema1.h"

#include <vector>
#include <iostream>

#include <Core/Engine.h>
#include "Transform2D.h"

using namespace std;

Tema1::Tema1() {

}

Tema1::~Tema1() {

}

Mesh* Tema1::CreateCircle(std::string name, glm::vec3 corner, int accuracy, float radius, glm::vec3 color, bool fill) {

	vector<VertexFormat> vertices = {
		VertexFormat(corner + glm::vec3(0, 0, 0), color),
		VertexFormat(corner + glm::vec3(radius, 0, 0), color),
	};
	vector<unsigned short> indices = {};

	for (int i = 1; i <= accuracy; i++) {
		float X = radius * sin(angle * i);
		float Y = radius * cos(angle * i);
		vertices.push_back(VertexFormat(corner + glm::vec3(X, Y, 0), color));
		indices.push_back(0);
		indices.push_back(i);
		indices.push_back(i + 1);
	}

	Mesh* circle = new Mesh(name);
	circle->InitFromData(vertices, indices);
	return circle;
}

Mesh* Tema1::CreateTube(std::string name, int size, float outline, int upDown) {

	glm::vec3 corner = glm::vec3(1366, -600, 0);

	size += 300;
	float width = 100.0f;

	// Corners translate values for outline
	glm::vec3 DL = glm::vec3(outline, outline, 0);
	glm::vec3 DR = glm::vec3(-outline, outline, 0);
	glm::vec3 UR = glm::vec3(-outline, -outline, 0);
	glm::vec3 UL = glm::vec3(outline, -outline, 0);

	glm::vec3 outlineColor = glm::vec3(0, 0, 0);

	// Colors
	glm::vec3 lighter = glm::vec3(0, 0.7f, 0);
	glm::vec3 darker = glm::vec3(0.6f, 1, 0.4f);

	// Shadowing effect - flip for upper tubes
	if (upDown == 1) {
		glm::vec3 aux = lighter;
		lighter = darker;
		darker = aux;
	}

	// Double the corners for outline effect
	std::vector<VertexFormat> vertices = {

		VertexFormat(corner + DL, darker),
		VertexFormat(corner + glm::vec3(width, 0, 0) + DR, lighter),
		VertexFormat(corner + glm::vec3(width, 2 * size, 0) + UR, lighter),
		VertexFormat(corner + glm::vec3(0, 2 * size, 0) + UL, darker),

		VertexFormat(corner + glm::vec3(0, 2 * size, 0) - glm::vec3(width / 4, 0, 0) + DL , darker),
		VertexFormat(corner + glm::vec3(width, 2 * size, 0) + glm::vec3(width / 4, 0, 0) + DR, lighter),
		VertexFormat(corner + glm::vec3(width, 2 * size, 0) + glm::vec3(width / 4, 0, 0) + glm::vec3(0, width / 4, 0) + UR, lighter),
		VertexFormat(corner + glm::vec3(0, 2 * size, 0) - glm::vec3(width / 4, 0, 0) + glm::vec3(0, width / 4, 0) + UL, darker),

		VertexFormat(corner, outlineColor),
		VertexFormat(corner + glm::vec3(width, 0, 0), outlineColor),
		VertexFormat(corner + glm::vec3(width, 2 * size, 0), outlineColor),
		VertexFormat(corner + glm::vec3(0, 2 * size, 0), outlineColor),

		VertexFormat(corner + glm::vec3(0, 2 * size, 0) - glm::vec3(width / 4, 0, 0), outlineColor),
		VertexFormat(corner + glm::vec3(width, 2 * size, 0) + glm::vec3(width / 4, 0, 0), outlineColor),
		VertexFormat(corner + glm::vec3(width, 2 * size, 0) + glm::vec3(width / 4, 0, 0) + glm::vec3(0, width / 4, 0), outlineColor),
		VertexFormat(corner + glm::vec3(0, 2 * size, 0) - glm::vec3(width / 4, 0, 0) + glm::vec3(0, width / 4, 0), outlineColor)
	};

	Mesh* tube = new Mesh(name);
	std::vector<unsigned short> indices = {
		0, 1, 2,
		2, 3, 0,

		4, 5, 6,
		6, 7, 4,

		8, 9, 10,
		10, 11, 8,

		12, 13, 14,
		14, 15, 12
	};

	tube->InitFromData(vertices, indices);
	return tube;
}

Mesh* Tema1::CreateCity(std::string name) {

	float mudHeight = resolution.y / 7;
	float grassHeight = mudHeight - 60;

	glm::vec3 corner = glm::vec3(0, mudHeight + grassHeight, 0);

	glm::vec3 DL = glm::vec3(1.5f, 1.5f, 0);
	glm::vec3 DR = glm::vec3(-1.5f, 1.5f, 0);
	glm::vec3 UR = glm::vec3(-1.5f, -1.5f, 0);
	glm::vec3 UL = glm::vec3(1.5f, -1.5f, 0);

	float width = 100.0f;

	glm::vec3 outlineColor = glm::vec3(1, 1, 1);
	glm::vec3 color = glm::vec3(0.85f, 0.85f, 0.85f);

	// Double the corners for outline effect
	std::vector<VertexFormat> vertices = {

		VertexFormat(corner + glm::vec3(10, 10, 0) + DL, color),
		VertexFormat(corner + glm::vec3(30, 10, 0) + DR, color),
		VertexFormat(corner + glm::vec3(30, 50, 0) + UR, color),
		VertexFormat(corner + glm::vec3(10, 50, 0) + UL, color),

		VertexFormat(corner + glm::vec3(20, 10, 0) + DL, color),
		VertexFormat(corner + glm::vec3(50, 10, 0) + DR, color),
		VertexFormat(corner + glm::vec3(50, 60, 0) + UR, color),
		VertexFormat(corner + glm::vec3(20, 60, 0) + UL, color),

		VertexFormat(corner + glm::vec3(40, 10, 0) + DL, color),
		VertexFormat(corner + glm::vec3(60, 10, 0) + DR, color),
		VertexFormat(corner + glm::vec3(60, 40, 0) + UR, color),
		VertexFormat(corner + glm::vec3(40, 40, 0) + UL, color),

		VertexFormat(corner + glm::vec3(10, 10, 0), outlineColor),
		VertexFormat(corner + glm::vec3(30, 10, 0), outlineColor),
		VertexFormat(corner + glm::vec3(30, 50, 0), outlineColor),
		VertexFormat(corner + glm::vec3(10, 50, 0), outlineColor),

		VertexFormat(corner + glm::vec3(20, 10, 0), outlineColor),
		VertexFormat(corner + glm::vec3(50, 10, 0), outlineColor),
		VertexFormat(corner + glm::vec3(50, 60, 0), outlineColor),
		VertexFormat(corner + glm::vec3(20, 60, 0), outlineColor),

		VertexFormat(corner + glm::vec3(40, 10, 0), outlineColor),
		VertexFormat(corner + glm::vec3(60, 10, 0), outlineColor),
		VertexFormat(corner + glm::vec3(60, 40, 0), outlineColor),
		VertexFormat(corner + glm::vec3(40, 40, 0), outlineColor),
	};

	Mesh* city = new Mesh(name);
	std::vector<unsigned short> indices = {
		0, 1, 2,
		2, 3, 0,


		12, 13, 14,
		14, 15, 12,

		8, 9, 10,
		10, 11, 8,

		20, 21, 22,
		22, 23, 20,

		4, 5, 6,
		6, 7, 4,

		16, 17, 18,
		18, 19, 16
	};

	city->InitFromData(vertices, indices);
	return city;
}

Mesh* Tema1::CreateGround(std::string name, int layer) {

	glm::vec3 color;

	float mudHeight = resolution.y / 7;
	float grassHeight = mudHeight - 60;

	glm::vec3 outlineColor = glm::vec3(0, 0, 0);
	glm::vec3 color_grass = glm::vec3(0, 0.9f, 0);

	// Choose underground (0) / grass (1)
	if (layer == 0) {
		color = glm::vec3(0.85f, 0.7f, 0.55f);
	}
	else {
		color = glm::vec3(0.6f, 1, 0.6f);
	}

	// Corners translate for outline effect
	glm::vec3 DL = glm::vec3(2, 2, 0);
	glm::vec3 DR = glm::vec3(-2, 2, 0);
	glm::vec3 UR = glm::vec3(-2, -2, 0);
	glm::vec3 UL = glm::vec3(2, -2, 0);

	// Double the corners for outline effect

	// Underground
	std::vector<VertexFormat> vertices0 = {

		VertexFormat(glm::vec3(0, 0, 0), color),
		VertexFormat(glm::vec3(resolution.x + grass_Slide, 0, 0), color),
		VertexFormat(glm::vec3(resolution.x + grass_Slide, mudHeight, 0), color),
		VertexFormat(glm::vec3(0, mudHeight, 0), color),
	};

	std::vector<unsigned short> indices0 = {
		0, 1, 2,
		2, 3, 0
	};

	// Grass initial
	std::vector<VertexFormat> vertices_aux =
	{
		VertexFormat(glm::vec3(0, mudHeight, 0) + DL, color),
		VertexFormat(glm::vec3(resolution.x + grass_Slide, mudHeight, 0) + DR, color),
		VertexFormat(glm::vec3(resolution.x + grass_Slide, mudHeight + grassHeight, 0) + UR, color),
		VertexFormat(glm::vec3(0, mudHeight + grassHeight, 0) + UL, color),

		VertexFormat(glm::vec3(0, mudHeight, 0), outlineColor),
		VertexFormat(glm::vec3(resolution.x + grass_Slide, mudHeight, 0), outlineColor),
		VertexFormat(glm::vec3(resolution.x + grass_Slide, mudHeight + grassHeight, 0), outlineColor),
		VertexFormat(glm::vec3(0, mudHeight + grassHeight, 0), outlineColor)
	};

	float X = 0;
	float Y1 = mudHeight + 2;
	float Y2 = mudHeight + grassHeight - 2;
	int i = 0;

	// Grass
	std::vector<VertexFormat> vertices1 = {};
	std::vector<unsigned short> indices1 = {};

	// Grass lawn mower effect
	while (X < resolution.x + grass_Slide) {
		vertices1.push_back(VertexFormat(glm::vec3(X, Y1, 0), color_grass));
		vertices1.push_back(VertexFormat(glm::vec3(X + 30, Y1, 0), color_grass));
		vertices1.push_back(VertexFormat(glm::vec3(X + 60, Y2, 0), color_grass));
		vertices1.push_back(VertexFormat(glm::vec3(X + 90, Y2, 0), color_grass));

		indices1.push_back(i);
		indices1.push_back(i + 1);
		indices1.push_back(i + 2);

		indices1.push_back(i + 2);
		indices1.push_back(i + 3);
		indices1.push_back(i + 1);

		X += 60;
		i += 4;
	}

	// Initial grass
	for (VertexFormat vertex : vertices_aux) {
		vertices1.push_back(vertex);
	}

	for (int j = 0; j < 2; j++) {
		indices1.push_back(i);
		indices1.push_back(i + 1);
		indices1.push_back(i + 2);

		indices1.push_back(i + 2);
		indices1.push_back(i + 3);
		indices1.push_back(i);

		i += 4;
	}

	Mesh* ground = new Mesh(name);

	if (layer == 0) {
		ground->InitFromData(vertices0, indices0);
	}
	else if (layer == 1) {
		ground->InitFromData(vertices1, indices1);
	}

	return ground;
}

Mesh* Tema1::CreateCloudsAndForest(std::string name) {

	float mudHeight = resolution.y / 7;
	float grassHeight = mudHeight - 60;

	glm::vec3 color;
	float entityHeight = 40;

	glm::vec3 corner = glm::vec3(0, mudHeight + grassHeight, 0);

	// Choose if object is city clouds / city forest
	if (name == "clouds") {
		color = glm::vec3(1, 1, 1);
		entityHeight = 125.0f;
	}
	else if (name == "forest") {
		color = glm::vec3(0, 0.7f, 0);
	}

	// Initial rectangle - on top of it will be added circles
	vector<VertexFormat> vertices = {
		VertexFormat(corner, color),
		VertexFormat(corner + glm::vec3(resolution.x, 0, 0), color),
		VertexFormat(corner + glm::vec3(resolution.x, entityHeight, 0), color),
		VertexFormat(corner + glm::vec3(0, entityHeight, 0), color)

	};
	vector<unsigned short> indices = {
		0, 1, 2,
		2, 3, 0
	};

	// Add circles for forest / clouds effect
	int counter = 4;
	int offset = -50;
	while (offset - 50 < resolution.x) {
		float radius = rand() % 40 - 0.5f;
		for (int i = counter; i <= counter + 32; i++) {
			float X = offset + radius * sin(angle * i);
			float Y = radius * cos(angle * i) + entityHeight;
			vertices.push_back(VertexFormat(corner + glm::vec3(X, Y, 0), color));
			indices.push_back(counter);
			indices.push_back(i);
			indices.push_back(i + 1);
		}
		offset += 15;
		counter += 32;
	}

	Mesh* clouds = new Mesh(name);
	clouds->InitFromData(vertices, indices);
	return clouds;
}

Mesh* Tema1::CreateBird(std::string name, int wings) {

	// Bird used colors
	glm::vec3 red = glm::vec3(1, 0, 0);
	glm::vec3 blue = glm::vec3(0, 0.5f, 1);
	glm::vec3 yellow = glm::vec3(1, 1, 0);

	// For eye / head circles
	int accuracy = 32;
	float radius = 17.5f;

	// Start drawing from the origin
	glm::vec3 corner = glm::vec3(0, 0, 0);

	vector<VertexFormat> vertices = {};
	vector<unsigned short> indices = {};

	// EYE
	vertices.push_back(VertexFormat(corner + glm::vec3(12.5f, 5, 0), red));
	vertices.push_back(VertexFormat(corner + glm::vec3(12.5f, 5, 0) + glm::vec3(radius/9, 0, 0), red));

	for (int i = 1; i <= accuracy; i++) {
		float X = radius / 9 * sin(angle * i);
		float Y = radius / 9 * cos(angle * i);
		vertices.push_back(VertexFormat(corner + glm::vec3(12.5f, 5, 0) + glm::vec3(X, Y, 0), red));
		indices.push_back(0);
		indices.push_back(i);
		indices.push_back(i + 1);
	}

	// HEAD
	vertices.push_back(VertexFormat(corner + glm::vec3(0, 0, 0), yellow));
	vertices.push_back(VertexFormat(corner + glm::vec3(radius, 0, 0), yellow));

	glm::vec3 beak = glm::vec3(0, 0, 0);

	for (int i = accuracy + 3; i <= 2 * accuracy + 3; i++) {
		float X = radius * sin(angle * i);
		float Y = radius * cos(angle * i);
		if (i == accuracy + 14) beak = corner + glm::vec3(X, Y, 0);
		if (corner.y + Y > max_Y) max_Y = corner.y + Y;
		vertices.push_back(VertexFormat(corner + glm::vec3(X, Y, 0), yellow));
		indices.push_back(accuracy + 2);
		indices.push_back(i);
		indices.push_back(i + 1);
	}

	// BEAK
	vertices.push_back(VertexFormat(beak + glm::vec3(18, -5, 0), blue));
	max_X = beak.x + 18;
	vertices.push_back(VertexFormat(beak + glm::vec3(0, 7.5f, 0), yellow));
	vertices.push_back(VertexFormat(beak + glm::vec3(-10, -7.5f, 0), blue));

	indices.push_back(2*accuracy + 5);
	indices.push_back(2*accuracy + 6);
	indices.push_back(2*accuracy + 7);

	// BODY
	vertices.push_back(VertexFormat(corner + glm::vec3(-90, -20, 0), red));
	vertices.push_back(VertexFormat(corner + glm::vec3(-30, -40, 0), yellow));
	min_Y = corner.y + (-40);

	indices.push_back(accuracy + 2);
	indices.push_back(2 * accuracy + 8);
	indices.push_back(2 * accuracy + 9);

	// LEGS
	vertices.push_back(VertexFormat(corner + glm::vec3(-70, -20, 0), yellow));
	vertices.push_back(VertexFormat(corner + glm::vec3(-50, -30, 0), blue));
	vertices.push_back(VertexFormat(corner + glm::vec3(-100, -30, 0), blue));
	min_X = corner.x + (-100);

	indices.push_back(2 * accuracy + 10);
	indices.push_back(2 * accuracy + 11);
	indices.push_back(2 * accuracy + 12);

	// WINGS - only if open
	if (wings == 1) {
		vertices.push_back(VertexFormat(corner + glm::vec3(-50, -20, 0), yellow));
		vertices.push_back(VertexFormat(corner + glm::vec3(-10, -10, 0), yellow));
		vertices.push_back(VertexFormat(corner + glm::vec3(-70, 0, 0), red));

		indices.push_back(2 * accuracy + 13);
		indices.push_back(2 * accuracy + 14);
		indices.push_back(2 * accuracy + 15);
	}

	Mesh* bird = new Mesh(name);

	bird->InitFromData(vertices, indices);
	return bird;
}

void Tema1::ResetStats() {

	totalTime = 0;

	glm::vec3 corner = glm::vec3(0, 0, 0);

	// Time before tubes start approaching and bird starts falling
	START_TIME = 200;

	SCORE = 0;

	// For wings effect
	fly_time = 0;

	// For Bird weight center calculus
	max_X = -1000;
	min_X = 1000;
	max_Y = -1000;
	min_Y = 1000;

	// Bird drop
	translateY = 0;
	total_Rotate = 0;
	total_Rotate_aux = 0;

	// Intial translation for X axis - for tubes
	translateX1 = -350;
	translateX2 = 0;
	translateX3 = 350;
	translateX4 = 700;

	// Random intial translation for Y axis - for tubes
	translateY1 = rand() % 150;
	translateY2 = rand() % 150;
	translateY3 = rand() % 150;
	translateY4 = rand() % 150;

	// Initiate collision coordinates for bird
	bird.x = 200;
	bird.y = 300;
	bird.width = 0;
	bird.height = 0;

	if (tubes.size() > 0) tubes.clear();
	// Initiate collision coordinates for tubes
	for (int i = 0; i < 8; i++) {
		Collision new_collision;
		if (i == 0 || i == 4) {
			new_collision.x = 1366 + translateX1;
			new_collision.y = -600 + translateY1;
		}
		else if (i == 1 || i == 5) {
			new_collision.x = 1366 + translateX2;
			new_collision.y = -600 + translateY2;
		}
		else if (i == 2 || i == 6) {
			new_collision.x = 1366 + translateX3;
			new_collision.y = -600 + translateY3;
		}
		else if (i == 3 || i == 7) {
			new_collision.x = 1366 + translateX4;
			new_collision.y = -600 + translateY4;
		}
		new_collision.width = 150.f;
		new_collision.height = 800;
		tubes.push_back(new_collision);
	}

	// Translate grass for distance effect
	translateGrass = 0;
}

void Tema1::Init() {

	resolution.x = 1280;
	resolution.y = 720;
	auto camera = GetSceneCamera();
	camera->SetOrthographic(0, (float)resolution.x, 0, (float)resolution.y, 0.01f, 400);
	camera->SetPosition(glm::vec3(0, 0, 50));
	camera->SetRotation(glm::vec3(0, 0, 0));
	camera->Update();
	GetCameraInput()->SetActive(false);

	ResetStats();

	glm::vec3 green = glm::vec3(0, 1, 0);
	
	Mesh* underground = CreateGround("underground", 0);
	AddMeshToList(underground);

	Mesh* grass = CreateGround("grass", 1);
	AddMeshToList(grass);

	Mesh* bird_closed = Tema1::CreateBird("bird_closed", 0);
	AddMeshToList(bird_closed);

	Mesh* bird_open = Tema1::CreateBird("bird_open", 1);
	AddMeshToList(bird_open);

	Mesh* bird = Tema1::CreateBird("bird", 0);
	AddMeshToList(bird);

	Mesh* circle = Tema1::CreateCircle("circle", glm::vec3(25, 25, 0), 100, 32, green, false);
	AddMeshToList(circle);

	Mesh* tube = Tema1::CreateTube("tube", 100, 2, 0);
	AddMeshToList(tube);

	Mesh* tube2 = Tema1::CreateTube("tube2", 100, 2, 1);
	AddMeshToList(tube2);

	Mesh* city = Tema1::CreateCity("city");
	AddMeshToList(city);

	Mesh* clouds = Tema1::CreateCloudsAndForest("clouds");
	AddMeshToList(clouds);

	Mesh* forest = Tema1::CreateCloudsAndForest("forest");
	AddMeshToList(forest);
}

void Tema1::FrameStart() {

	// Clears the color buffer (using the previously set color) and depth buffer
	glClearColor(0, 0.8f, 1, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::ivec2 resolution = window->GetResolution();
	// Sets the screen area where to draw
	glViewport(0, 0, resolution.x, resolution.y);
}

void Tema1::Update(float deltaTimeSeconds) {

	totalTime++;
	fly_time++;

	// For collision calculus
	bird.x = 200;
	bird.y = 300 + translateY;
	
	for (int i = 0; i < 8; i++) {
		if (i == 0 || i == 4) {
			tubes[i].x = 1366 + translateX1;
			tubes[i].y = -590 + translateY1;
			if (i == 4) {
				tubes[i].y += (775 + 2 * sizeOfTube);
			}
		}
		else if (i == 1 || i == 5) {
			tubes[i].x = 1366 + translateX2;
			tubes[i].y = -590 + translateY2;
			if (i == 5) {
				tubes[i].y += (775 + 2 * sizeOfTube);
			}
		}
		else if (i == 2 || i == 6) {
			tubes[i].x = 1366 + translateX3;
			tubes[i].y = -590 + translateY3;
			if (i == 6) {
				tubes[i].y += (775 + 2 * sizeOfTube);
			}
		}
		else if (i == 3 || i == 7) {
			tubes[i].x = 1366 + translateX4;
			tubes[i].y = -590 + translateY4;
			if (i == 7) {
				tubes[i].y += (775 + 2 * sizeOfTube);
			}
		}
	}

	// Collision check for every tube
	for (int i = 0; i < 8; i++) {
		if (bird.x < tubes[i].x + tubes[i].width &&
			bird.x + bird.width > tubes[i].x &&
			bird.y < tubes[i].y + tubes[i].height &&
			bird.y + bird.height > tubes[i].y) {

			cout << "GAME OVER! FINAL SCORE:" << SCORE << endl << endl;

			totalTime = 0;

			Init();
		}
		if (bird.y > 550 || bird.y < 25) {
			cout << "GAME OVER! FINAL SCORE:" << SCORE << endl << endl;

			totalTime = 0;

			Init();
		}
	}


	// Score increment and print
	if ((-translateX1 - 200) - 1080 < 10 && (-translateX1 - 200) - 1080 > 8) {
		SCORE++;
		cout << "SCORE: " << SCORE << endl;
	}
	if ((-translateX2 - 200) - 1080 < 10 && (-translateX2 - 200) - 1080 > 8) {
		SCORE++;
		cout << "SCORE: " << SCORE << endl;
	}
	if ((-translateX3 - 200) - 1080 < 10 && (-translateX3 - 200) - 1080 > 8) {
		SCORE++;
		cout << "SCORE: " << SCORE << endl;
	}
	if ((-translateX4 - 200) - 1080 < 10 && (-translateX4 - 200) - 1080 > 8) {
		SCORE++;
		cout << "SCORE: " << SCORE << endl;
	}


	// Reset tubes translations
	if (- translateX1 - 200 > 1280) {
		translateX1 = -70;
		translateY1 = rand() % 100;
	}
	if (-translateX2 - 200 > 1280 || (translateX1 == -350 && totalTime > START_TIME + 25)) {
		translateX2 = -70;
		translateY2 = rand() % 100;
	}
	if (-translateX3 - 200 > 1280 || translateX2 == -350) {
		translateX3 = -70;
		translateY3 = rand() % 100;
	}
	if (-translateX4 - 200 > 1280 || translateX3 == -350) {
		translateX4 = -70;
		translateY4 = rand() % 100;
	}


	// Grass slide effect
	if (-translateGrass > grass_Slide) {
		translateGrass = 0;
	}

	glLineWidth(10);

	if (totalTime > START_TIME) {
		translateX1 -= deltaTimeSeconds * 125.f;
		translateX2 -= deltaTimeSeconds * 125.f;
		translateX3 -= deltaTimeSeconds * 125.f;
		translateX4 -= deltaTimeSeconds * 125.f;
		translateGrass -= deltaTimeSeconds * 125.f;
		translateY += SPACE_PRESS / 10 - deltaTimeSeconds * 125.0f;
	}


	// BIRD
	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Scale(1.25f, 1.25f);
	modelMatrix *= Transform2D::Translate(200, 300);

	if (totalTime > START_TIME) {
		modelMatrix *= Transform2D::Translate(0, translateY);

		if (total_Rotate > M_PI / 3) {
			total_Rotate = M_PI / 3;
		}

		modelMatrix *= Transform2D::Translate((min_X + max_X) / 2, (min_Y + max_Y) / 2);
		modelMatrix *= Transform2D::Rotate(-total_Rotate);
		modelMatrix *= Transform2D::Translate(-(min_X + max_X) / 2, -(min_Y + max_Y) / 2);

		if (SPACE_PRESS == 100) {
			modelMatrix *= Transform2D::Translate((min_X + max_X) / 2, (min_Y + max_Y) / 2);
			modelMatrix *= Transform2D::Rotate(total_Rotate_aux + 0.02f);
			modelMatrix *= Transform2D::Translate(-(min_X + max_X) / 2, -(min_Y + max_Y) / 2);
			total_Rotate_aux = 0;
		}
		else {
			total_Rotate += 0.02f;
		}

	}

	// Bird wings effect
	if (fly_time < 10 || totalTime < START_TIME) {
		RenderMesh2D(meshes["bird_closed"], shaders["VertexColor"], modelMatrix);
	}
	else {
		RenderMesh2D(meshes["bird_open"], shaders["VertexColor"], modelMatrix);
	}

	if (fly_time > 20) {
		fly_time = 0;
	}


	// SPACE PRESS
	if (SPACE_PRESS > 0) SPACE_PRESS -= 10;
	if (SPACE_PRESS < 0) SPACE_PRESS = 0;



	// UPPER TUBE 1
	modelMatrix = glm::mat3(1);

	modelMatrix *= Transform2D::Translate((1366 + translateX1 + sizeOfTube / 2), (sizeOfTube + 12.5f));
	modelMatrix *= Transform2D::Rotate(M_PI);
	modelMatrix *= Transform2D::Translate(-(1366 + translateX1 + sizeOfTube / 2), -(sizeOfTube + 12.5f));

	modelMatrix *= Transform2D::Translate(0, -(700 - 2 * sizeOfTube - 5));
	modelMatrix *= Transform2D::Translate(translateX1, -translateY1);
	RenderMesh2D(meshes["tube2"], shaders["VertexColor"], modelMatrix);

	// UPPER TUBE 2
	modelMatrix = glm::mat3(1);

	modelMatrix *= Transform2D::Translate((1366 + translateX2 + sizeOfTube / 2), (sizeOfTube + 12.5f));
	modelMatrix *= Transform2D::Rotate(M_PI);
	modelMatrix *= Transform2D::Translate(-(1366 + translateX2 + sizeOfTube / 2), -(sizeOfTube + 12.5f));

	modelMatrix *= Transform2D::Translate(0, -(700 - 2 * sizeOfTube - 5));
	modelMatrix *= Transform2D::Translate(translateX2, -translateY2);
	RenderMesh2D(meshes["tube2"], shaders["VertexColor"], modelMatrix);

	// UPPER TUBE 3
	modelMatrix = glm::mat3(1);

	modelMatrix *= Transform2D::Translate((1366 + translateX3 + sizeOfTube / 2), (sizeOfTube + 12.5f));
	modelMatrix *= Transform2D::Rotate(M_PI);
	modelMatrix *= Transform2D::Translate(-(1366 + translateX3 + sizeOfTube / 2), -(sizeOfTube + 12.5f));

	modelMatrix *= Transform2D::Translate(0, -(700 - 2 * sizeOfTube - 5));
	modelMatrix *= Transform2D::Translate(translateX3, -translateY3);
	RenderMesh2D(meshes["tube2"], shaders["VertexColor"], modelMatrix);

	// UPPER TUBE 4
	modelMatrix = glm::mat3(1);

	modelMatrix *= Transform2D::Translate((1366 + translateX4 + sizeOfTube / 2), (sizeOfTube + 12.5f));
	modelMatrix *= Transform2D::Rotate(M_PI);
	modelMatrix *= Transform2D::Translate(-(1366 + translateX4 + sizeOfTube / 2), -(sizeOfTube + 12.5f));

	modelMatrix *= Transform2D::Translate(0, -(700 - 2 * sizeOfTube - 5));
	modelMatrix *= Transform2D::Translate(translateX4, -translateY4);
	RenderMesh2D(meshes["tube2"], shaders["VertexColor"], modelMatrix);



	// MUD AND GRASS
	modelMatrix = glm::mat3(1);

	RenderMesh2D(meshes["underground"], shaders["VertexColor"], modelMatrix);

	modelMatrix *= Transform2D::Translate(translateGrass, 0);
	RenderMesh2D(meshes["grass"], shaders["VertexColor"], modelMatrix);



	// LOWER TUBE 1
	modelMatrix = glm::mat3(1);

	modelMatrix *= Transform2D::Translate(translateX1, translateY1);
	RenderMesh2D(meshes["tube"], shaders["VertexColor"], modelMatrix);

	// LOWER TUBE 2
	modelMatrix = glm::mat3(1);

	modelMatrix *= Transform2D::Translate(translateX2, translateY2);
	RenderMesh2D(meshes["tube"], shaders["VertexColor"], modelMatrix);

	// LOWER TUBE 3
	modelMatrix = glm::mat3(1);

	modelMatrix *= Transform2D::Translate(translateX3, translateY3);
	RenderMesh2D(meshes["tube"], shaders["VertexColor"], modelMatrix);

	// LOWER TUBE 4
	modelMatrix = glm::mat3(1);

	modelMatrix *= Transform2D::Translate(translateX4, translateY4);
	RenderMesh2D(meshes["tube"], shaders["VertexColor"], modelMatrix);



	// FOREST
	modelMatrix = glm::mat3(1);

	RenderMesh2D(meshes["forest"], shaders["VertexColor"], modelMatrix);



	// CITIES
	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Scale(2.25f, 2.25f);

	modelMatrix *= Transform2D::Translate(50, -80);
	RenderMesh2D(meshes["city"], shaders["VertexColor"], modelMatrix);

	modelMatrix *= Transform2D::Translate(100, 0);
	RenderMesh2D(meshes["city"], shaders["VertexColor"], modelMatrix);

	modelMatrix *= Transform2D::Translate(100, 0);
	RenderMesh2D(meshes["city"], shaders["VertexColor"], modelMatrix);

	modelMatrix *= Transform2D::Translate(100, 0);
	RenderMesh2D(meshes["city"], shaders["VertexColor"], modelMatrix);

	modelMatrix *= Transform2D::Translate(100, 0);
	RenderMesh2D(meshes["city"], shaders["VertexColor"], modelMatrix);



	// CLOUDS
	modelMatrix = glm::mat3(1);

	RenderMesh2D(meshes["clouds"], shaders["VertexColor"], modelMatrix);
}

void Tema1::FrameEnd() {

}

void Tema1::OnInputUpdate(float deltaTime, int mods) {

}

void Tema1::OnKeyPress(int key, int mods) {

	// total_Rotate_aux used to rotate back the bird horizontally
	if (key == GLFW_KEY_SPACE && totalTime > START_TIME) {
		SPACE_PRESS = 100;
		total_Rotate_aux = total_Rotate;
		total_Rotate = 0;
	}
}

void Tema1::OnKeyRelease(int key, int mods) {

}

void Tema1::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY) {

}

void Tema1::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods) {

}

void Tema1::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods) {

}

void Tema1::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY) {

}

void Tema1::OnWindowResize(int width, int height) {

}
