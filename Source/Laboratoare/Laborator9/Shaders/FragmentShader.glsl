#version 330
 
uniform sampler2D texture_1;
uniform sampler2D texture_2;
 
in vec2 texcoord;
flat in int number_of_textures_fragment;

layout(location = 0) out vec4 out_color;

void main()
{
	// TODO : calculate the out_color using the texture2D() function
	vec4 color1 = texture2D(texture_1, texcoord);
	if (color1.a < .5) discard;
	vec4 color2 = texture2D(texture_2, texcoord);
	// if (color2.a < .5) discard;

	if (number_of_textures_fragment == 0) {
		out_color = color1;
	} else {
		out_color = mix(color1, color2, 0.5f);
	}
}