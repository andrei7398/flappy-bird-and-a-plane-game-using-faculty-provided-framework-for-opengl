#include "Laborator4.h"

#include <vector>
#include <string>
#include <iostream>

#include <Core/Engine.h>
#include "Transform3D.h"

using namespace std;

Laborator4::Laborator4()
{
}

Laborator4::~Laborator4()
{
}

void Laborator4::Init()
{
	polygonMode = GL_FILL;

	Mesh* mesh = new Mesh("box");
	mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "box.obj");
	meshes[mesh->GetMeshID()] = mesh;

	// initialize tx, ty and tz (the translation steps)
	translateX = 0;
	translateY = 0;
	translateZ = 0;

	// initialize sx, sy and sz (the scale factors)
	scaleX = 1;
	scaleY = 1;
	scaleZ = 1;

	// initialize angularSteps
	angularStepOX = 0;
	angularStepOY = 0;
	angularStepOZ = 0;
}

void Laborator4::FrameStart()
{
	// clears the color buffer (using the previously set color) and depth buffer
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::ivec2 resolution = window->GetResolution();
	// sets the screen area where to draw
	glViewport(0, 0, resolution.x, resolution.y);
}

float k = 0.05;
float Tk = 0.03;
float xp, yp, zp;
float xp2, yp2, zp2;
float x2 = 1, y2 = 1;

void Laborator4::Update(float deltaTimeSeconds)
{
	glLineWidth(3);
	glPointSize(5);
	glPolygonMode(GL_FRONT_AND_BACK, polygonMode);

	modelMatrix = glm::mat4(1);
	modelMatrix *= Transform3D::Translate(-2.5f, 0.5f, -1.5f);
	modelMatrix *= Transform3D::Translate(translateX, translateY, translateZ);
	//RenderMesh(meshes["box"], shaders["VertexNormal"], modelMatrix);

	translateY += k;

	if (translateY >= 5) {
		k = k * -1;
	}
	if (translateY < -5) {
		k = k * -1;
	}

	modelMatrix = glm::mat4(1);
	modelMatrix *= Transform3D::Translate(0.0f, 0.5f, -1.5f);
	modelMatrix *= Transform3D::Scale(scaleX, scaleY, scaleZ);
	RenderMesh(meshes["box"], shaders["Simple"], modelMatrix);

	scaleX += k;
	scaleY += k;
	scaleZ += k;

	if (scaleX >= 3) {
		k = k * -1;
	}
	if (scaleX < 0) {
		k = k * -1;
	}

	modelMatrix = glm::mat4(1);
	//	modelMatrix *= Transform3D::Translate(xp, yp, zp);
	modelMatrix *= Transform3D::RotateOZ(-xp);
	modelMatrix *= Transform3D::Translate(-3, 0, 0);

	xp = xp + M_PI * deltaTimeSeconds;
	// dt1 * s + dt2 *s + ... + dtn * s =  s * (dt1 + dt2 +... + dtn) = s * 1
	//Point startPoint = new Point(subtractButton.Size);
	RenderMesh(meshes["box"], shaders["VertexNormal"], modelMatrix);


	if (xp2 < 180) {
		xp2 = xp2 + M_PI * deltaTimeSeconds;
	}
	else {
		xp2 = 0;
	}
	
	modelMatrix = glm::mat4(1);
	modelMatrix *= Transform3D::RotateOZ(-xp2);
	modelMatrix *= Transform3D::Translate(x2, y2, 0);
	// modelMatrix *= Transform3D::Scale(scaleX, scaleY, scaleZ);
	// RenderMesh(meshes["box"], shaders["Simple"], modelMatrix);

	float auxX = x2 * cos(xp) - y2 * sin(xp);
	float auxY = y2 * cos(xp) + x2 * sin(xp);

	x2 = auxX;
	y2 = auxY;
}

void Laborator4::FrameEnd()
{
	DrawCoordinatSystem();
}

void Laborator4::OnInputUpdate(float deltaTime, int mods)
{
	// TODO
}

void Laborator4::OnKeyPress(int key, int mods)
{
	// add key press event
	if (key == GLFW_KEY_SPACE)
	{
		switch (polygonMode)
		{
		case GL_POINT:
			polygonMode = GL_FILL;
			break;
		case GL_LINE:
			polygonMode = GL_POINT;
			break;
		default:
			polygonMode = GL_LINE;
			break;
		}
	}
}

void Laborator4::OnKeyRelease(int key, int mods)
{
	// add key release event
}

void Laborator4::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{
	// add mouse move event
}

void Laborator4::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button press event
}

void Laborator4::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button release event
}

void Laborator4::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY)
{
}

void Laborator4::OnWindowResize(int width, int height)
{
}
