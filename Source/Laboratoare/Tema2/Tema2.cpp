#include "Tema2.h"

#include <vector>
#include <string>
#include <iostream>
#include "../Laborator4/Transform3D.h"

#include <Core/Engine.h>

using namespace std;

Tema2::Tema2() {

}

Tema2::~Tema2() {

}

Mesh* Tema2::CreateMesh(const char* name, const std::vector<VertexFormat>& vertices, const std::vector<unsigned short>& indices) {

	unsigned int VAO = 0;
	// Create the VAO and bind it
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	// Create the VBO and bind it
	unsigned int VBO;
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	// Send vertices data into the VBO buffer
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices[0]) * vertices.size(), &vertices[0], GL_STATIC_DRAW);

	// Crete the IBO and bind it
	unsigned int IBO;
	glGenBuffers(1, &IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);

	// Send indices data into the IBO buffer
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * indices.size(), &indices[0], GL_STATIC_DRAW);

	// set vertex position attribute
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), 0);

	// set vertex normal attribute
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(sizeof(glm::vec3)));

	// set texture coordinate attribute
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(2 * sizeof(glm::vec3)));

	// set vertex color attribute
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(2 * sizeof(glm::vec3) + sizeof(glm::vec2)));

	// set elapsed time attribute
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(4, 1, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(3 * sizeof(glm::vec3) + sizeof(glm::vec2)));

	// Unbind the VAO
	glBindVertexArray(0);

	// Check for OpenGL errors
	CheckOpenGLError();

	// Mesh information is saved into a Mesh object
	meshes[name] = new Mesh(name);
	meshes[name]->InitFromBuffer(VAO, static_cast<unsigned short>(indices.size()));
	meshes[name]->vertices = vertices;
	meshes[name]->indices = indices;
	return meshes[name];
}

Mesh* Tema2::CreatePropeller(std::string name, float length, float thickness) {

	glm::vec3 green = glm::vec3(0, 1, 0);
	glm::vec3 red = glm::vec3(1, 0, 0);
	glm::vec3 blue = glm::vec3(0, 0, 1);
	glm::vec3 black = glm::vec3(0, 0, 0);
	glm::vec3 white = glm::vec3(1, 1, 1);
	std::vector <glm::vec3> color_pallet = { green, red, blue, black, white };

	float cst = length / 4;

	vector<VertexFormat> vertices {
		VertexFormat(glm::vec3(-length, -cst, thickness), black, glm::vec3(0.2, 0.8, 0.2)),
		VertexFormat(glm::vec3(length, -cst, thickness), black, glm::vec3(0.9, 0.4, 0.2)),
		VertexFormat(glm::vec3(-length, cst, thickness), black, glm::vec3(0.7, 0.7, 0.1)),
		VertexFormat(glm::vec3(length, cst, thickness), black, glm::vec3(0.7, 0.3, 0.7)),
		VertexFormat(glm::vec3(-length, -cst, -thickness), black, glm::vec3(0.3, 0.5, 0.4)),
		VertexFormat(glm::vec3(length, -cst, -thickness), black, glm::vec3(0.5, 0.2, 0.9)),
		VertexFormat(glm::vec3(-length, cst, -thickness), black, glm::vec3(0.7, 0.0, 0.7)),
		VertexFormat(glm::vec3(length, cst, -thickness), black, glm::vec3(0.1, 0.5, 0.8)),

		VertexFormat(glm::vec3(-cst, -length, thickness), black, glm::vec3(0.2, 0.8, 0.2)),
		VertexFormat(glm::vec3(cst, -length, thickness), black, glm::vec3(0.9, 0.4, 0.2)),
		VertexFormat(glm::vec3(-cst, length, thickness), black, glm::vec3(0.7, 0.7, 0.1)),
		VertexFormat(glm::vec3(cst, length, thickness), black, glm::vec3(0.7, 0.3, 0.7)),
		VertexFormat(glm::vec3(-cst, -length, -thickness), black, glm::vec3(0.3, 0.5, 0.4)),
		VertexFormat(glm::vec3(cst, -length, -thickness), black, glm::vec3(0.5, 0.2, 0.9)),
		VertexFormat(glm::vec3(-cst, length, -thickness), black, glm::vec3(0.7, 0.0, 0.7)),
		VertexFormat(glm::vec3(cst, length, -thickness), black, glm::vec3(0.1, 0.5, 0.8))
	};

	vector<unsigned short> indices = {
		0, 1, 2,		1, 3, 2,
		2, 3, 7,		2, 7, 6,
		1, 7, 3,		1, 5, 7,
		6, 7, 4,		7, 5, 4,
		0, 4, 1,		1, 4, 5,
		2, 6, 4,		0, 2, 4,

		8, 9, 10,		9, 11, 10,
		10, 11, 15,		10, 15, 14,
		9, 15, 11,		9, 13, 15,
		14, 15, 12,		15, 13, 12,
		8, 12, 9,		9, 12, 13,
		10, 14, 12,		8, 10, 12
	};

	return CreateMesh("propeller", vertices, indices);
}

Mesh* Tema2::CreatePlane(std::string name, float length) {

	vector<VertexFormat> vertices = {};
	vector<unsigned short> indices = {};

	int x1 = 0, x2 = 0;
	int y1 = 0, y2 = 0;

	vector<unsigned short> rectangularPrism_indices = {
		0, 1, 2,		1, 3, 2,
		2, 3, 7,		2, 7, 6,
		1, 7, 3,		1, 5, 7,
		6, 7, 4,		7, 5, 4,
		0, 4, 1,		1, 4, 5,
		2, 6, 4,		0, 2, 4
	};

	glm::vec3 green = glm::vec3(0, 1, 0);
	glm::vec3 red = glm::vec3(1, 0, 0);
	glm::vec3 blue = glm::vec3(0, 0, 1);
	glm::vec3 black = glm::vec3(0, 0, 0);
	glm::vec3 white = glm::vec3(1, 1, 1);

	float radius = 1.0f;
	float angle = M_PI / 58;

	// Draw 2 disks
	for (int j = 0; j < 2; j++) {
		float actual_radius = j == 1 ? radius : 0.6f * radius;
		y1 = 0;
		y2 = 2 * radius;
		x1 = j * length;
		vertices.push_back(VertexFormat(glm::vec3(0, 0, j * length), red, glm::vec3(0, 0, 0)));
		vertices.push_back(VertexFormat(glm::vec3(actual_radius, 0, j * length), red, glm::vec3(0, 0, 0)));

		int start = j * 120 + 1;
		int end = (j + 1) * 120;

		for (int i = start; i < end; i++) {
			float X = actual_radius * sin(angle * i);
			float Y = actual_radius * cos(angle * i);
			vertices.push_back(VertexFormat(glm::vec3(X, Y, j * length), red, glm::vec3(0, 0, 0)));
			indices.push_back(j * 121);
			indices.push_back(i);
			indices.push_back(i + 1);
		}
	}

	// Unite the disks
	for (int i = 1; i < 120; i++) {
		indices.push_back(i);
		indices.push_back(i + 121);
		indices.push_back(i + 121 + 1);

		indices.push_back(i + 121 + 1);
		indices.push_back(i + 1);
		indices.push_back(i);
	}

	// Con in fata
	plane_tip = 1.25f * length;
	vertices.push_back(VertexFormat(glm::vec3(0, 0, plane_tip), red, glm::vec3(0, 0, 0)));
	x2 = plane_tip;

	for (int i = 121; i < vertices.size() - 1; i++) {
		indices.push_back(vertices.size() - 1);
		indices.push_back(i);
		indices.push_back(i + 1);
	}

	// WINGS
	float wing_distance = radius / 2;
	float wingWidthStart = length / 3;
	float wingWidthEnd = 2 * length / 3;
	float wingThickness = radius / 8;

	// Upper WING
	int verticesOffset_Wing1 = vertices.size();

	vertices.push_back(VertexFormat(glm::vec3(-length, -wingThickness + wing_distance, wingWidthEnd), black, glm::vec3(0.2, 0.8, 0.2)));
	vertices.push_back(VertexFormat(glm::vec3(length, -wingThickness + wing_distance, wingWidthEnd), black, glm::vec3(0.9, 0.4, 0.2)));
	vertices.push_back(VertexFormat(glm::vec3(-length, wingThickness + wing_distance, wingWidthEnd), black, glm::vec3(0.7, 0.7, 0.1)));
	vertices.push_back(VertexFormat(glm::vec3(length, wingThickness + wing_distance, wingWidthEnd), black, glm::vec3(0.7, 0.3, 0.7)));
	vertices.push_back(VertexFormat(glm::vec3(-length, -wingThickness + wing_distance, wingWidthStart), black, glm::vec3(0.3, 0.5, 0.4)));
	vertices.push_back(VertexFormat(glm::vec3(length, -wingThickness + wing_distance, wingWidthStart), black, glm::vec3(0.5, 0.2, 0.9)));
	vertices.push_back(VertexFormat(glm::vec3(-length, wingThickness + wing_distance, wingWidthStart), black, glm::vec3(0.7, 0.0, 0.7)));
	vertices.push_back(VertexFormat(glm::vec3(length, wingThickness + wing_distance, wingWidthStart), black, glm::vec3(0.1, 0.5, 0.8)));

	for (unsigned short wingVertex : rectangularPrism_indices) {
		indices.push_back(wingVertex + verticesOffset_Wing1);
	}

	// Lower WING
	int verticesOffset_Wing2 = vertices.size();

	vertices.push_back(VertexFormat(glm::vec3(-length, -wingThickness - wing_distance, wingWidthEnd), black, glm::vec3(0.2, 0.8, 0.2)));
	vertices.push_back(VertexFormat(glm::vec3(length, -wingThickness - wing_distance, wingWidthEnd), black, glm::vec3(0.9, 0.4, 0.2)));
	vertices.push_back(VertexFormat(glm::vec3(-length, wingThickness - wing_distance, wingWidthEnd), black, glm::vec3(0.7, 0.7, 0.1)));
	vertices.push_back(VertexFormat(glm::vec3(length, wingThickness - wing_distance, wingWidthEnd), black, glm::vec3(0.7, 0.3, 0.7)));
	vertices.push_back(VertexFormat(glm::vec3(-length, -wingThickness - wing_distance, wingWidthStart), black, glm::vec3(0.3, 0.5, 0.4)));
	vertices.push_back(VertexFormat(glm::vec3(length, -wingThickness - wing_distance, wingWidthStart), black, glm::vec3(0.5, 0.2, 0.9)));
	vertices.push_back(VertexFormat(glm::vec3(-length, wingThickness - wing_distance, wingWidthStart), black, glm::vec3(0.7, 0.0, 0.7)));
	vertices.push_back(VertexFormat(glm::vec3(length, wingThickness - wing_distance, wingWidthStart), black, glm::vec3(0.1, 0.5, 0.8)));

	for (unsigned short wingVertex : rectangularPrism_indices) {
		indices.push_back(wingVertex + verticesOffset_Wing2);
	}

	// WING HOLDERS
	float wingH_WidthStart = wingWidthStart;
	float wingH_WidthEnd = wingWidthEnd;

	// Wing holder right
	int verticesOffset_WingH_R = vertices.size();

	float wingH_R_LengthStart = -length + (length / 10);
	float wingH_R_LengthEnd = -length + 2 * (length / 10);

	vertices.push_back(VertexFormat(glm::vec3(wingH_R_LengthStart, -wingThickness - wing_distance + 0.1f, wingH_WidthEnd - 0.1f), red, glm::vec3(0.2, 0.8, 0.2)));
	vertices.push_back(VertexFormat(glm::vec3(wingH_R_LengthEnd, -wingThickness - wing_distance + 0.1f, wingH_WidthEnd - 0.1f), red, glm::vec3(0.9, 0.4, 0.2)));
	vertices.push_back(VertexFormat(glm::vec3(wingH_R_LengthStart, wingThickness + wing_distance - 0.1f, wingH_WidthEnd - 0.1f), red, glm::vec3(0.7, 0.7, 0.1)));
	vertices.push_back(VertexFormat(glm::vec3(wingH_R_LengthEnd, wingThickness + wing_distance - 0.1f, wingH_WidthEnd - 0.1f), red, glm::vec3(0.7, 0.3, 0.7)));
	vertices.push_back(VertexFormat(glm::vec3(wingH_R_LengthStart, -wingThickness - wing_distance + 0.1f, wingH_WidthStart + 0.1f), red, glm::vec3(0.3, 0.5, 0.4)));
	vertices.push_back(VertexFormat(glm::vec3(wingH_R_LengthEnd, -wingThickness - wing_distance + 0.1f, wingH_WidthStart + 0.1f), red, glm::vec3(0.5, 0.2, 0.9)));
	vertices.push_back(VertexFormat(glm::vec3(wingH_R_LengthStart, wingThickness + wing_distance - 0.1f, wingH_WidthStart + 0.1f), red, glm::vec3(0.7, 0.0, 0.7)));
	vertices.push_back(VertexFormat(glm::vec3(wingH_R_LengthEnd, wingThickness + wing_distance - 0.1f, wingH_WidthStart + 0.1f), red, glm::vec3(0.1, 0.5, 0.8)));

	for (unsigned short wingVertex : rectangularPrism_indices) {
		indices.push_back(wingVertex + verticesOffset_WingH_R);
	}

	// Wing holder left
	int verticesOffset_WingH_L = vertices.size();

	float wingH_L_LengthStart = length - (length / 10) - 0.1f;
	float wingH_L_LengthEnd = length - 2 * (length / 10) + 0.1f;

	vertices.push_back(VertexFormat(glm::vec3(wingH_L_LengthStart, -wingThickness - wing_distance + 0.1f, wingH_WidthEnd - 0.1f), red, glm::vec3(0.2, 0.8, 0.2)));
	vertices.push_back(VertexFormat(glm::vec3(wingH_L_LengthEnd, -wingThickness - wing_distance + 0.1f, wingH_WidthEnd - 0.1f), red, glm::vec3(0.9, 0.4, 0.2)));
	vertices.push_back(VertexFormat(glm::vec3(wingH_L_LengthStart, wingThickness + wing_distance - 0.1f, wingH_WidthEnd - 0.1f), red, glm::vec3(0.7, 0.7, 0.1)));
	vertices.push_back(VertexFormat(glm::vec3(wingH_L_LengthEnd, wingThickness + wing_distance - 0.1f, wingH_WidthEnd + 0.1f), red, glm::vec3(0.7, 0.3, 0.7)));
	vertices.push_back(VertexFormat(glm::vec3(wingH_L_LengthStart, -wingThickness - wing_distance + 0.1f, wingH_WidthStart + 0.1f), red, glm::vec3(0.3, 0.5, 0.4)));
	vertices.push_back(VertexFormat(glm::vec3(wingH_L_LengthEnd, -wingThickness - wing_distance + 0.1f, wingH_WidthStart + 0.1f), red, glm::vec3(0.5, 0.2, 0.9)));
	vertices.push_back(VertexFormat(glm::vec3(wingH_L_LengthStart, wingThickness + wing_distance - 0.1f, wingH_WidthStart + 0.1f), red, glm::vec3(0.7, 0.0, 0.7)));
	vertices.push_back(VertexFormat(glm::vec3(wingH_L_LengthEnd, wingThickness + wing_distance - 0.1f, wingH_WidthStart + 0.1f), red, glm::vec3(0.1, 0.5, 0.8)));

	for (unsigned short wingVertex : rectangularPrism_indices) {
		indices.push_back(wingVertex + verticesOffset_WingH_L);
	}

	// Tail
	int verticesOffset_Tail = vertices.size();

	vertices.push_back(VertexFormat(glm::vec3(radius * 0.4f, length / 5, 0), black));
	vertices.push_back(VertexFormat(glm::vec3(radius * 0.4f, 0, 0.1f), black));
	vertices.push_back(VertexFormat(glm::vec3(radius * 0.4f, 0, length / 4), black));

	indices.push_back(verticesOffset_Tail);
	indices.push_back(verticesOffset_Tail + 1);
	indices.push_back(verticesOffset_Tail + 2);

	vertices.push_back(VertexFormat(glm::vec3(- radius * 0.4f, length / 5, 0), black));
	vertices.push_back(VertexFormat(glm::vec3(- radius * 0.4f, 0, 0.1f), black));
	vertices.push_back(VertexFormat(glm::vec3(- radius * 0.4f, 0, length / 4), black));

	indices.push_back(verticesOffset_Tail + 3);
	indices.push_back(verticesOffset_Tail + 4);
	indices.push_back(verticesOffset_Tail + 5);

	indices.push_back(verticesOffset_Tail);
	indices.push_back(verticesOffset_Tail + 3);
	indices.push_back(verticesOffset_Tail + 4);

	indices.push_back(verticesOffset_Tail + 4);
	indices.push_back(verticesOffset_Tail + 1);
	indices.push_back(verticesOffset_Tail);

	indices.push_back(verticesOffset_Tail);
	indices.push_back(verticesOffset_Tail + 3);
	indices.push_back(verticesOffset_Tail + 5);

	indices.push_back(verticesOffset_Tail + 5);
	indices.push_back(verticesOffset_Tail + 2);
	indices.push_back(verticesOffset_Tail);

	plane_collision.x = (x1 + x2) / 2;
	plane_collision.y = (y1 + y2) / 2;

	return CreateMesh("plane", vertices, indices);
}

Mesh* Tema2::CreateSea(std::string name, float length)
{

	vector<VertexFormat> vertices = {};
	vector<unsigned short> indices = {};

	glm::vec3 color = glm::vec3(0.6f, 0.9f, 1);


	float radius = 1.0f;
	float angle = M_PI / 58;

	// 5 disks at equal distance
	for (int j = 0; j < 485; j += 121) {
		int circle_index = j / 121;
		int circle_distance = length / 5;

		vertices.push_back(VertexFormat(glm::vec3(0, 0, circle_index * circle_distance), color, color));
		vertices.push_back(VertexFormat(glm::vec3(radius, 0, circle_index * circle_distance), color, color));

		for (int i = j; i < j + 119; i++) {
			float X = radius * sin(angle * (i - j));
			float Y = radius * cos(angle * (i - j));
			vertices.push_back(VertexFormat(glm::vec3(X, Y, circle_index * circle_distance), color, color));
			indices.push_back(j);
			indices.push_back(i);
			indices.push_back(i + 1);
		}
	}

	// unite all disks
	for (int j = 0; j < vertices.size(); j += 121) {
		for (int i = j + 1; i < j + 119; i++) {
			indices.push_back(i);
			indices.push_back(i + 121);
			indices.push_back(i + 121 + 1);

			indices.push_back(i + 121 + 1);
			indices.push_back(i + 1);
			indices.push_back(i);
		}
	}

	return CreateMesh("sea", vertices, indices);
}

void Tema2::Init() {

	camera_Rotation = glm::vec3(2.2f, -1.6f, -2.2f);
	camera_Position = glm::vec3(-27, 15, 0.043f);

	auto camera = GetSceneCamera();

	camera->SetRotation(camera_Rotation);
	camera->SetPosition(camera_Position);
	camera->Update();

	GetCameraInput()->SetActive(false);

	camera_changed = false;

	position = glm::vec3(0, 0, 0);

	angularStep = 0;

	plane_collision.x -= 2.5f;
	plane_collision.y += 18;

	{
		Mesh* mesh = new Mesh("sphere");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "sphere.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	{
		Shader* shader = new Shader("ShaderSea");
		shader->AddShader("Source/Laboratoare/Tema2/Shaders/VertexShaderTema2.glsl", GL_VERTEX_SHADER);
		shader->AddShader("Source/Laboratoare/Tema2/Shaders/FragmentShaderTema2.glsl", GL_FRAGMENT_SHADER);
		shader->CreateAndLink();
		shaders[shader->GetName()] = shader;
	}

	{
		Shader* shader = new Shader("ShaderNormal");
		shader->AddShader("Source/Laboratoare/Tema2/Shaders/VertexShaderNormal.glsl", GL_VERTEX_SHADER);
		shader->AddShader("Source/Laboratoare/Tema2/Shaders/FragmentShaderNormal.glsl", GL_FRAGMENT_SHADER);
		shader->CreateAndLink();
		shaders[shader->GetName()] = shader;
	}

	{
		CreateSea("sea", 5);
		CreatePlane("plane", 5);
		CreatePropeller("propeller", 3, 0.1f);
	}
}


void Tema2::FrameStart() {

	glClearColor(0, 1, 1, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::ivec2 resolution = window->GetResolution();
	glViewport(0, 0, resolution.x, resolution.y);

	resolution_Y = resolution.y;
}

void Tema2::Update(float deltaTimeSeconds) {

	time++;

	if (camera_changed == true) {
		auto camera = GetSceneCamera();

		camera->SetRotation(camera_Rotation);
		camera->SetPosition(camera_Position);
		camera->Update();

		camera_changed = false;
	}

	float aux = deltaTimeSeconds;
	if (fmod(deltaTimeSeconds, 100) == 0) sign = !sign;

	if (FIRST_PERSON && abs(oscilation) > 2) {
		oscialation_sign = -oscialation_sign;
	}
	if (!FIRST_PERSON && abs(oscilation) > 2) {
		oscialation_sign = -oscialation_sign;
	}

	angularStep += deltaTimeSeconds * 2.f;

	// SEA
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix *= Transform3D::Translate(-25, 0, 0);
		modelMatrix *= Transform3D::RotateOY(sign * M_PI / 2);

		modelMatrix *= Transform3D::Scale(26, 13, 26);

		RenderSimpleMesh(meshes["sea"], shaders["ShaderSea"], modelMatrix);
	}

	// CLOUDS
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix *= Transform3D::RotateOX(-rotateObstacles);
		rotateObstacles = rotateObstacles + (M_PI * deltaTimeSeconds) / 25;
		modelMatrix *= Transform3D::Translate(-5, -29, 0);
		glm::mat4 first = modelMatrix;
		glm::mat4 second = modelMatrix;
		glm::mat4 third = modelMatrix;
		glm::mat4 fourth = modelMatrix;
		first *= Transform3D::Scale(3, 3, 3);
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], first, glm::vec3(0, 0, 0.5f));
		second *= Transform3D::Translate(-0.5f, 0, 1);
		second *= Transform3D::Scale(1.5f, 1.5f, 1.5f);
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], second, glm::vec3(0, 0, 0.8f));
		third *= Transform3D::Translate(-0.25f, 0, 2);
		third *= Transform3D::Scale(3.5f, 3.5f, 3.5f);
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], third, glm::vec3(0, 0, 0.5f));
		fourth *= Transform3D::Translate(1, 0, 3);
		fourth *= Transform3D::Scale(2, 2, 2);
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], fourth, glm::vec3(0, 0, 0.8f));
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix *= Transform3D::RotateOX(-rotateObstacles - 10);
		rotateObstacles = rotateObstacles + (M_PI * deltaTimeSeconds) / 25;
		modelMatrix *= Transform3D::Translate(5, -29, 0);
		glm::mat4 first = modelMatrix;
		glm::mat4 second = modelMatrix;
		glm::mat4 third = modelMatrix;
		glm::mat4 fourth = modelMatrix;
		first *= Transform3D::Scale(3, 3, 3);
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], first, glm::vec3(0, 0, 0.5f));
		second *= Transform3D::Translate(-0.5f, 0, 1);
		second *= Transform3D::Scale(1.5f, 1.5f, 1.5f);
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], second, glm::vec3(0, 0, 0.8f));
		third *= Transform3D::Translate(-0.25f, 0, 2);
		third *= Transform3D::Scale(3, 3, 3);
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], third, glm::vec3(0, 0, 0.9f));
		fourth *= Transform3D::Translate(1, 0, 3);
		fourth *= Transform3D::Scale(2, 2, 2);
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], fourth, glm::vec3(0, 0, 0.8f));
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix *= Transform3D::RotateOX(-rotateObstacles - 20);
		rotateObstacles = rotateObstacles + (M_PI * deltaTimeSeconds) / 25;
		modelMatrix *= Transform3D::Translate(-4, -29, 0);
		glm::mat4 first = modelMatrix;
		glm::mat4 second = modelMatrix;
		glm::mat4 third = modelMatrix;
		glm::mat4 fourth = modelMatrix;
		first *= Transform3D::Scale(3, 3, 3);
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], first, glm::vec3(0, 0, 0.7f));
		second *= Transform3D::Translate(-0.5f, 0, 1);
		second *= Transform3D::Scale(1.25f, 1.25f, 1.25f);
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], second, glm::vec3(0, 0, 0.8f));
		third *= Transform3D::Translate(-0.25f, 0, 2);
		third *= Transform3D::Scale(2.5f, 2.5f, 2.5f);
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], third, glm::vec3(0, 0, 0.7f));
		fourth *= Transform3D::Translate(1, 0, 3);
		fourth *= Transform3D::Scale(2, 2, 2);
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], fourth, glm::vec3(0, 0, 0.4f));
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix *= Transform3D::RotateOX(-rotateObstacles - 30);
		rotateObstacles = rotateObstacles + (M_PI * deltaTimeSeconds) / 25;
		modelMatrix *= Transform3D::Translate(4, -29, 0);
		glm::mat4 first = modelMatrix;
		glm::mat4 second = modelMatrix;
		glm::mat4 third = modelMatrix;
		glm::mat4 fourth = modelMatrix;
		first *= Transform3D::Scale(3, 3, 3);
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], first, glm::vec3(0, 0, 0.7f));
		second *= Transform3D::Translate(-0.5f, 0, 1);
		second *= Transform3D::Scale(1.25f, 1.25f, 1.25f);
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], second, glm::vec3(0, 0, 0.4f));
		third *= Transform3D::Translate(-0.25f, 0, 2);
		third *= Transform3D::Scale(2.5f, 2.5f, 2.5f);
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], third, glm::vec3(0, 0, 0.6f));
		fourth *= Transform3D::Translate(1, 0, 3);
		fourth *= Transform3D::Scale(2, 2, 2);
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], fourth, glm::vec3(0, 0, 0.8f));
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix *= Transform3D::RotateOX(-rotateObstacles - 40);
		rotateObstacles = rotateObstacles + (M_PI * deltaTimeSeconds) / 25;
		modelMatrix *= Transform3D::Translate(-7, -29, 0);
		glm::mat4 first = modelMatrix;
		glm::mat4 second = modelMatrix;
		glm::mat4 third = modelMatrix;
		glm::mat4 fourth = modelMatrix;
		first *= Transform3D::Scale(3, 3, 3);
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], first, glm::vec3(0, 0, 0.7f));
		second *= Transform3D::Translate(-0.5f, 0, 1);
		second *= Transform3D::Scale(1.25f, 1.25f, 1.25f);
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], second, glm::vec3(0, 0, 0.8f));
		third *= Transform3D::Translate(-0.25f, 0, 2);
		third *= Transform3D::Scale(2.5f, 2.5f, 2.5f);
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], third, glm::vec3(0, 0, 0.7f));
		fourth *= Transform3D::Translate(1, 0, 3);
		fourth *= Transform3D::Scale(2, 2, 2);
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], fourth, glm::vec3(0, 0, 0.8f));
	}

	if (time > 300)
	{
		// first obstacle
		glm::mat4 modelMatrix = glm::mat4(1);

		glm::vec3 red = glm::vec3(1, 0, 0);

		float osc_const = 0.01f;

		int random;

		if (time > 325) {
			modelMatrix *= Transform3D::RotateOX(-rotateObstacles2);
			rotateObstacles2 = rotateObstacles2 + (M_PI * deltaTimeSeconds) / 5;
			oscilation = oscilation + oscialation_sign * osc_const;
			random = rand() % 3;
			if (FIRST_PERSON) {
				modelMatrix *= Transform3D::Translate(oscilation, -21 - oscilation, 0);
			}
			else {
				modelMatrix *= Transform3D::Translate(0, -21 - oscilation, 0);
			}
			first_collision.x = sin(-rotateObstacles2) * (-21 - oscilation);
			first_collision.y = -21 - oscilation;
			RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], modelMatrix, red);
		}

		// 2nd obstacle
		modelMatrix = glm::mat4(1);
		modelMatrix *= Transform3D::RotateOX(-rotateObstacles2 - 1);
		oscilation = oscilation - oscialation_sign * osc_const;
		random = rand() % 3;
		if (FIRST_PERSON) {
			modelMatrix *= Transform3D::Translate(oscilation, -21 + oscilation, 0);
		}
		else {
			modelMatrix *= Transform3D::Translate(0, -21 + oscilation, 0);
		}
		second_collision.x = sin(-rotateObstacles2) * (-21 + oscilation);
		second_collision.y = -21 + oscilation;
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], modelMatrix, red);

		//// 3rd obstacle
		//modelMatrix = glm::mat4(1);
		//modelMatrix *= Transform3D::RotateOX(-rotateObstacles2 - 2);
		//oscilation = oscilation + oscialation_sign * osc_const;
		//random = rand() % 3;
		//if (FIRST_PERSON) {
		//	modelMatrix *= Transform3D::Translate(oscilation, -21 - oscilation, 0);
		//}
		//else {
		//	modelMatrix *= Transform3D::Translate(0, -21 - oscilation, 0);
		//}
		//third_collision.x = sin(-rotateObstacles2) * (-21 - oscilation);
		//third_collision.y = -21 - oscilation;
		//RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], modelMatrix, red);

		//// 4th obstacle
		//modelMatrix = glm::mat4(1);
		//modelMatrix *= Transform3D::RotateOX(-rotateObstacles2 - 3);
		//oscilation = oscilation - oscialation_sign * osc_const;
		//random = rand() % 3;
		//if (FIRST_PERSON) {
		//	modelMatrix *= Transform3D::Translate(oscilation, -21 + oscilation, 0);
		//}
		//else {
		//	modelMatrix *= Transform3D::Translate(0, -21 + oscilation, 0);
		//}
		//fourth_collision.x = sin(-rotateObstacles2) * (-21 + oscilation);
		//fourth_collision.y = -21 + oscilation;
		//RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], modelMatrix, red);

		//// 5th obstacle
		//modelMatrix = glm::mat4(1);
		//modelMatrix *= Transform3D::RotateOX(-rotateObstacles2 - 4);
		//oscilation = oscilation + oscialation_sign * osc_const;
		//random = rand() % 3;
		//if (FIRST_PERSON) {
		//	modelMatrix *= Transform3D::Translate(oscilation, -21 - oscilation, 0);
		//}
		//else {
		//	modelMatrix *= Transform3D::Translate(0, -21 - oscilation, 0);
		//}
		//fifth_collision.x = sin(-rotateObstacles2) * (-21 - oscilation);
		//fifth_collision.y = -21 - oscilation;
		//RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], modelMatrix, red);
	}
	
	// LIVES
	{
		glm::mat4 modelMatrix = glm::mat3(1);

		glm::vec3 red = glm::vec3(0.8f, 0, 0);
		
		if (FIRST_PERSON) {
			if (lives > 0) {
				modelMatrix *= Transform3D::Translate(12, 26, 0);
				RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], modelMatrix, red);
				if (lives > 1) {
					modelMatrix *= Transform3D::Translate(-1.5f, -0.1f, 0);
					RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], modelMatrix, red);
					if (lives > 2) {
						modelMatrix *= Transform3D::Translate(-1.5f, -0.1f, 0);
						RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], modelMatrix, red);
					}
				}
			}
		} else {
			if (lives > 0) {
				modelMatrix *= Transform3D::Translate(-15, 21, -11.5f);
				RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], modelMatrix, red);
				if (lives > 1) {
					modelMatrix *= Transform3D::Translate(0, 0, 1.5f);
					RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], modelMatrix, red);
					if (lives > 2) {
						modelMatrix *= Transform3D::Translate(0, 0, 1.5f);
						RenderSimpleMesh(meshes["sphere"], shaders["ShaderNormal"], modelMatrix, red);
					}
				}
			}
		}
	}

	{
		x += 0.01f;
	}

	AirplaneMovement();

	// PROPELLER
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix *= Transform3D::Translate(0, mouseCoord - airplane_inclination * 20, 0);
		modelMatrix *= Transform3D::Translate(0, 18, plane_tip - 2.75f);
		modelMatrix *= Transform3D::Scale(0.5f, 0.5f, 0.5f);

		modelMatrix *= Transform3D::RotateOX(M_PI * airplane_inclination);
		modelMatrix *= Transform3D::RotateOZ(angularStep * M_PI);
		RenderSimpleMesh(meshes["propeller"], shaders["ShaderNormal"], modelMatrix, glm::vec3(1, 1, 1));
	}

	// PLANE
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix *= Transform3D::Translate(0, mouseCoord, 0);
		modelMatrix *= Transform3D::Translate(0, 18, -2.5f);

		plane_collision.y = -21 + collision_coord;

		modelMatrix *= Transform3D::RotateOX(M_PI * airplane_inclination);
		RenderSimpleMesh(meshes["plane"], shaders["ShaderNormal"], modelMatrix, glm::vec3(1, 1, 1));
	}

	if (touch > 0) {
		touch--;
	}

	if (time > 500) {
		if ((plane_collision.x > first_collision.x && -plane_collision.x < first_collision.x) && plane_collision.y - first_collision.y < 0.25f && touch == 0 && abs(remainder(rotateObstacles2, 6)) > 2.5f) {
			cout << plane_collision.x << " " << plane_collision.y << endl;
			cout << first_collision.x << " " << first_collision.y << endl;
			touch = 100;
			lives--;
		}
		if ((plane_collision.x > second_collision.x && -plane_collision.x < second_collision.x) && plane_collision.y - second_collision.y < 0.25f && touch == 0 && abs(remainder(rotateObstacles2, 6)) > 2.5f) {
			cout << plane_collision.x << " " << plane_collision.y << endl;
			cout << second_collision.x << " " << second_collision.y << endl;
			touch = 100;
			lives--;
		}
		if ((plane_collision.x > third_collision.x && -plane_collision.x < third_collision.x) && plane_collision.y - third_collision.y < 0.25f && touch == 0 && abs(remainder(rotateObstacles2, 6)) > 2.5f) {
			touch = 100;
			lives--;
		}
		if ((plane_collision.x > fourth_collision.x && -plane_collision.x < fourth_collision.x) && plane_collision.y - fourth_collision.y < 0.25f && touch == 0 && abs(remainder(rotateObstacles2, 6)) > 2.5f) {
			touch = 100;
			lives--;
		}
		if ((plane_collision.x > fifth_collision.x && -plane_collision.x < fifth_collision.x) && plane_collision.y - fifth_collision.y < 0.25f && touch == 0 && abs(remainder(rotateObstacles2, 6)) > 2.5f) {
			touch = 100;
			lives--;
		}
	}
}

void Tema2::FrameEnd() {

	// DrawCoordinatSystem();
}

void Tema2::AirplaneMovement() {

	if (mouseCoord < -3) mouseCoord = -3;
	if (mouseCoord > 8.5f) mouseCoord = 8.5f;

	if (mouseCoord > last_mouseCoord && airplane_inclination > -0.1f) {
		airplane_inclination -= 0.001f;
	}
	else if (mouseCoord < last_mouseCoord && airplane_inclination < 0.1f) {
		airplane_inclination += 0.001f;
	}
	else if (mouseCoord == last_mouseCoord || ((mouseCoord > last_mouseCoord || mouseCoord < last_mouseCoord) && abs(airplane_inclination) == 0.1f)) {
		if (airplane_inclination < 0) {
			airplane_inclination += 0.001f;
		}
		else if (airplane_inclination > 0) {
			airplane_inclination -= 0.001f;
		}
	}

	last_mouseCoord = mouseCoord;
}

void Tema2::RenderSimpleMesh(Mesh* mesh, Shader* shader, const glm::mat4& modelMatrix, const glm::vec3& color)
{
	if (!mesh || !shader || !shader->GetProgramID())
		return;

	glUseProgram(shader->program);

	int location = glGetUniformLocation(shader->GetProgramID(), "Model");
	glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(modelMatrix));

	location = glGetUniformLocation(shader->GetProgramID(), "View");
	glm::mat4 viewMatrix = GetSceneCamera()->GetViewMatrix();
	glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(viewMatrix));

	location = glGetUniformLocation(shader->GetProgramID(), "Projection");
	glm::mat4 projectionMatrix = GetSceneCamera()->GetProjectionMatrix();
	glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(projectionMatrix));

	int color_input = glGetUniformLocation(shader->program, "color_input");
	glUniform3f(color_input, color.r, color.g, color.b);

	location = glGetUniformLocation(shader->GetProgramID(), "Time");
	glUniform1f(location, Engine::GetElapsedTime());

	glBindVertexArray(mesh->GetBuffers()->VAO);
	glDrawElements(mesh->GetDrawMode(), static_cast<int>(mesh->indices.size()), GL_UNSIGNED_SHORT, 0);
}

void Tema2::OnInputUpdate(float deltaTime, int mods) {

}

void Tema2::OnKeyPress(int key, int mods) {

	if (key == GLFW_KEY_P) {
		camera_Position.x = camera_Position.x == -27 ? -0.35f : -27;
		camera_Position.y = camera_Position.y == 15 ? 25 : 15;
		camera_Position.z = camera_Position.z == 0.043f ? -13 : 0.043f;

		camera_Rotation.x = camera_Rotation.x == 2.2f ? 2.7f : 2.2f;
		camera_Rotation.y = camera_Rotation.y == -1.6f ? -0.070f : -1.6f;
		camera_Rotation.z = camera_Rotation.z == -2.2f ? -3.1f : -2.2f;

		camera_changed = true;
		FIRST_PERSON = !FIRST_PERSON;
	}
}

void Tema2::OnKeyRelease(int key, int mods) {

}

void Tema2::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY) {

	int divider = resolution_Y / 5;

	if (divider == 0) {
		collision_coord = -2;
	}
	else if (mouseY / divider == 1) {
		collision_coord = -1;
	}
	else if (mouseY / divider == 2) {
		collision_coord = 0;
	}
	else if (mouseY / divider == 3) {
		collision_coord = 1;
	}
	else if (mouseY / divider >= 4) {
		collision_coord = 2;
	}


	last_mouseCoord = mouseCoord;

	mouseCoord -= (float) deltaY / (float) 75;

	if (mouseCoord < -3) mouseCoord = -3;
	if (mouseCoord > 8.5f) mouseCoord = 8.5f;

	if (mouseCoord > last_mouseCoord && airplane_inclination > -0.1f) {
		airplane_inclination -= 0.005f;
	}
	else if (mouseCoord < last_mouseCoord && airplane_inclination < 0.1f) {
		airplane_inclination += 0.005f;
	}
	else if (mouseCoord == last_mouseCoord || ((mouseCoord > last_mouseCoord || mouseCoord < last_mouseCoord) && abs(airplane_inclination) == 0.1f)) {
		if (airplane_inclination < 0) {
			airplane_inclination += 0.005f;
		}
		else if (airplane_inclination > 0) {
			airplane_inclination -= 0.005f;
		}
	}
}

void Tema2::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods) {

}

void Tema2::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods) {

}

void Tema2::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY) {

}

void Tema2::OnWindowResize(int width, int height) {

}

