#version 330

// TODO: get color value from vertex shader
in vec3 world_position;
in vec3 world_normal;

// Uniforms for light properties
uniform vec3 light_direction;
uniform vec3 light_position;
uniform vec3 eye_position;

uniform float material_kd;
uniform float material_ks;
uniform int material_shininess;

uniform vec3 object_color;

layout(location = 0) out vec4 out_color;

uniform int isSpot;
uniform float angle;

void main()
{
	// TODO: define ambient light component
	vec3 N = normalize(world_normal);
	vec3 L = normalize(light_position - world_position);
	vec3 V = normalize(eye_position - world_position);
	vec3 H = normalize(L + V);
	vec3 R = reflect(-L, N);

	float ambient_light = material_kd * 0.25;
	float diffuse_light = material_kd * max (dot (L, N), 0);
	float specular_light = material_ks * pow (max (dot (R,V), 0), material_shininess);

	float spot_light = dot(-L, light_direction);
	float spot_light_limit = cos(radians(30));

	vec3 light;

	float linear_att = (spot_light - spot_light_limit) / (1 - spot_light_limit);
	float light_att_factor = pow (linear_att, 2);
	float aten_fac = 1 / (pow (distance (light_position, world_position), 2));

	if (isSpot == 0) {
		light = object_color * (ambient_light +  1.2f * (diffuse_light + specular_light));
	} else {
		if (spot_light > cos(radians(30))) {
			light = object_color * (ambient_light + (light_att_factor * aten_fac) * (diffuse_light + specular_light));
		} else {
			light = ambient_light * object_color;
		}
	}

	// TODO: write pixel out color
	out_color = vec4(light, 1);
}