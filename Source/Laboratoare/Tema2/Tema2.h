#pragma once
#include <Component/SimpleScene.h>
#include <Core/GPU/Mesh.h>

class Tema2 : public SimpleScene
{
	public:
		Tema2();
		~Tema2();

		void Init() override;

		Mesh * CreateMesh(const char * name, const std::vector<VertexFormat> &vertices, const std::vector<unsigned short> &indices);

		void AirplaneMovement();

	private:
		void FrameStart() override;
		void Update(float deltaTimeSeconds) override;
		void FrameEnd() override;

		void RenderSimpleMesh(Mesh *mesh, Shader *shader, const glm::mat4 &modelMatrix, const glm::vec3& color = glm::vec3(1));

		void OnInputUpdate(float deltaTime, int mods) override;
		void OnKeyPress(int key, int mods) override;
		void OnKeyRelease(int key, int mods) override;
		void OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY) override;
		void OnMouseBtnPress(int mouseX, int mouseY, int button, int mods) override;
		void OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods) override;
		void OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY) override;
		void OnWindowResize(int width, int height) override;

		Mesh* CreateSea(std::string name, float length);
		Mesh* CreatePlane(std::string name, float length);
		Mesh* CreatePropeller(std::string name, float length, float thickness);

protected:
	float time;
	GLenum polygonMode;
	glm::vec3 position;
	float last_mouseCoord;
	float mouseCoord;
	float angularStep;

	float plane_tip;

	glm::vec3 plane_collision;

	glm::vec3 first_collision = glm::vec3(1000, 1000, 1000);
	glm::vec3 second_collision = glm::vec3(1000, 1000, 1000);
	glm::vec3 third_collision = glm::vec3(1000, 1000, 1000);
	glm::vec3 fourth_collision = glm::vec3(1000, 1000, 1000);
	glm::vec3 fifth_collision = glm::vec3(1000, 1000, 1000);

	int collision_coord;
	int resolution_Y;

	bool camera_changed;
	glm::vec3 camera_Position;
	glm::vec3 camera_Rotation;

	float x = 0;
	bool sign = true;
	float oscialation_sign = 1;

	float rotateObstacles = 0;
	float rotateObstacles2 = 0;
	float oscilation;

	bool FIRST_PERSON = false;

	float airplane_inclination = 0;

	int lives = 3;

	int touch = 0;
};
